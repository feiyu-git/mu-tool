const path = require('path');
// 在打包后的 dist 目录下新建 index.html ，并且将打包后的 js 文件自动引入到 index.html 中
// const HtmlWebpackPlugin = require("html-webpack-plugin");
// 打包前删除之前的 dsit
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

// CommonJS 语法
module.exports = {
  // 告诉webpack使用production模式的内置优化,开启代码压缩，否则会添加很多注释
  mode: "production",
  // 入口文件
  entry: path.join(__dirname, 'src', 'index.js'),
  output: {
    // 定义filename便于script标签引入
    filename: 'index.js',
    chunkFilename: '[name].[contenthash].chunk.js',
    path: path.resolve(__dirname, 'dist'),
    publicPath: './',
    // 下面是打包成 labrary 时才需要
    library: 'muTool',
    libraryExport: "default", // 对外暴露default属性，就可以直接调用default里的属性
    globalObject: 'this', // 定义全局变量,兼容node和浏览器运行，避免出现"window is not defined"的情况
    libraryTarget: 'umd' // 定义打包方式Universal Module Definition,同时支持在CommonJS、AMD和全局变量使用
  },
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        include: path.resolve(__dirname, 'src'),
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      }
    ]
  },
  plugins: [
    // 工具库不需要 index.html
    // new HtmlWebpackPlugin({
    //   template: path.join(__dirname, 'public/index.html'),
    //   filename: 'index.html'
    // }),
    new CleanWebpackPlugin()
  ],
  devServer: {
    port: 8000,
    static: path.join(__dirname, "dist")
  }
}