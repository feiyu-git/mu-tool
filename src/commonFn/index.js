/**
 * JS颜色十六进制转换为rgb或rgba,返回的格式为 rgba（255，255，255，0.5）字符串
 * sHex为传入的十六进制的色值
 * alpha为rgba的透明度
 */
export function hex2Rgba(sHex, alpha = 1) {
  // 十六进制颜色值的正则表达式
  const reg = /^#([0-9a-fA-f]{3}|[0-9a-fA-f]{6})$/;
  /* 16进制颜色转为RGB格式 */
  let sColor = sHex.toLowerCase();
  if (sColor && reg.test(sColor)) {
    if (sColor.length === 4) {
      let sColorNew = '#';
      for (let i = 1; i < 4; i += 1) {
        sColorNew += sColor.slice(i, i + 1).concat(sColor.slice(i, i + 1));
      }
      sColor = sColorNew;
    }
    //  处理六位的颜色值
    const sColorChange = [];
    for (let i = 1; i < 7; i += 2) {
      sColorChange.push(parseInt(`0x${sColor.slice(i, i + 2)}`, 16));
    }
    return `rgba(${sColorChange.join(',')},${alpha})`;
  }
  return sColor;
}

/**
 *  生成任意长度随机数（包括字母和数字）
 */
export function createRandomString(len) {
  const charSet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  let randomString = '';
  for (let i = 0; i < len; i += 1) {
    const randomPoz = Math.floor(Math.random() * charSet.length);
    randomString += charSet.substring(randomPoz, randomPoz + 1);
  }
  return randomString;
}

/**
 * 设置随机颜色
 */
const PresetColorPool = ['#d9e6c3', '#ebd5be', '#d1e6de', '#e0ceeb', '#ebd3c7', '#e6dada', '#e3deeb', '#dedae6', '#cad0e8', '#cedeeb'];
let RestColorPool = PresetColorPool.concat([]);
// 获取颜色
// 如果不传指定位置，返回随机颜色，保证在所有ColorArray颜色使用完后，再重新随机返回颜色
export function getPresetColor(idx) {
  let val;
  if (typeof idx === 'number') {
    val = PresetColorPool[idx % PresetColorPool.length];
  } else {
    const len = RestColorPool.length;
    if (len === 0) {
      RestColorPool = PresetColorPool.concat([]);
    }
    const index = Math.floor(Math.random() * len);
    val = RestColorPool[index];
    RestColorPool.splice(index, 1);
  }
  return val;
}
