export const TAG = ['boy', 'girl']

export function sumFn(a, b) {
  if (!a || !b) console.log('请传入正确的参数');
  return a + b;
}