import * as commonFn from './commonFn/index'
// import { hex2Rgba, createRandomString,getPresetColor } from './commonFn/index'
import * as test from './commonFn/test'

// 这个格式不要改！否则在目标工程中无法访问到导出的函数
export default {
  ...commonFn,
  ...test
}