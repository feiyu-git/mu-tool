# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.0.12](https://gitlab.com/feiyu-git/mu-tool/compare/v1.0.11...v1.0.12) (2022-07-03)

### [1.0.11](https://gitlab.com/feiyu-git/mu-tool/compare/v1.0.10...v1.0.11) (2022-07-03)

### [1.0.10](https://gitlab.com/feiyu-git/mu-tool/compare/v1.0.9...v1.0.10) (2022-07-03)

### [1.0.9](https://gitlab.com/feiyu-git/mu-tool/compare/v1.0.8...v1.0.9) (2022-07-03)

### [1.0.8](https://gitlab.com/feiyu-git/mu-tool/compare/v1.0.7...v1.0.8) (2022-07-03)

### [1.0.7](https://gitlab.com/feiyu-git/mu-tool/compare/v1.0.6...v1.0.7) (2022-07-03)

### [1.0.6](https://gitlab.com/feiyu-git/mu-tool/compare/v1.0.5...v1.0.6) (2022-07-02)

### [1.0.5](https://gitlab.com/feiyu-git/mu-tool/compare/v1.0.4...v1.0.5) (2022-07-02)

### [1.0.4](https://gitlab.com/feiyu-git/mu-tool/compare/v1.0.3...v1.0.4) (2022-06-19)

### [1.0.3](https://gitlab.com/feiyu-git/mu-tool/compare/v1.0.2...v1.0.3) (2022-06-19)

### [1.0.2](https://gitlab.com/feiyu-git/mu-tool/compare/v1.0.1...v1.0.2) (2022-06-19)

### [1.0.1](https://gitlab.com/feiyu-git/mu-tool/compare/v1.0.1-beta.0...v1.0.1) (2022-06-18)

### 1.0.1-beta.0 (2022-06-18)
